'use strict'
var express = require('express');
var app = express();

var fs = require('fs');
var mime = require('mime');
var multer  = require('multer');
var upload = multer({ dest : 'tmp/' });

app.listen(8080, function () {
  console.log('Example app listening on port 8080!');
});

app.get('/form.html', function (request, response) {
  var options = {
    root: './',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };
  response.sendFile("form.html", options, function (err) {
    if (err) throw(err);
    console.log('Loaded form page');
  });
});

app.get('/display.html', function (request, response) {
  var options = {
    root: './',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };
  response.sendFile("display.html", options, function (err) {
    if (err) throw(err);
    console.log('Loaded display page');
  });
});

app.get('/display', function (request, response) {
  var filePath =  "uploads/";
  fs.readdir(filePath, function(err, files){
    if (err) throw(err);
    var filenames = [];
    for( var f of files ) {
      var fileName = f.split(".")[0];
      if( filenames.indexOf(fileName) == -1 ) {
        filenames.push(fileName);
      }
    }
    response.send(filenames);
  });
});

app.get('/uploads/*', function (request, response) {
  var options = {
    root: './uploads',
    dotfiles: 'deny',
    headers: {
        'x-timestamp': Date.now(),
        'x-sent': true
    }
  };
  var path = request.params[0];
  response.sendFile(path, options, function (err) {
    if (err) throw(err);
    console.log('Sent: ', path);
  });
});

// Saves any image and text that's uploaded
app.post('/upload', upload.single('image'), function(request, response) {
  var target_name = "uploads/file" + Date.now();
  var ext = request.file.mimetype;
  var imageExts = ["image/png"];
  if( imageExts.indexOf(ext) == -1 ) {
    fs.unlink(request.file.path, function(err) {
      if (err) throw(err);
      response.send("Not an allowed file type. PNGs only.")
    });
  }
  else {
    if( request.file.size > 2097152 ) {
      response.send("File is too large.")
    }
    else {
      // save text using Date.now() to make name unique
      var target_file = target_name + ".txt";
      fs.writeFile(target_file, request.body.text, 'utf8', function(err) {
        // save image with same name
        target_file = target_name + "." + mime.extension(ext);
        fs.rename(request.file.path, target_file, function(err) {
          if (err) throw(err);
          response.send('Upload complete.');
        });
      });
    }
  }
});
