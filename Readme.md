# M7 Technical Article Code

To run the code and install the necessary modules, you need **node.js** and **npm**.  
To install the modules run the command **npm install**.   
To run, in the terminal enter the command **node server.js**, then open a browser window and enter **http://localhost:8080/form.html** in the navigation bar.
You can also navigate to **http://localhost:8080/display.html**.
